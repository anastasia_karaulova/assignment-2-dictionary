%define tag 0
%macro colon 2
    %2: dq tag
    db %1, 0
    %define tag %2
%endmacro
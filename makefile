ASM=nasm
FLAGS=-felf64
LINK.o = ld -o $@ $^

main: main.o dict.o lib.o
	$(LINK.o)

main.o: main.asm lib.inc colon.inc
	$(ASM) $(FLAGS) -o $@ $<
	
%.o: %.asm
	$(ASM) $(FLAGS) -o $@ $<

.PHONY: clean

clean:
	$(RM) *.o
	$(RM) main
%include "colon.inc"
%include "words.inc"
%include "lib.inc"
%define MAX_SIZE 255
%define SHIFT 8
%define SIZE_ERROR 3
%define NOT_FOUND_ERROR 4
%define 

section .rodata
buffer_size_error: db "Превышена допустимая длина в 255 символов", `\n`, 0
not_found:         db "Элемент не найден", `\n`, 0

section .text
extern find_word
global _start

_start:

	sub rsp, MAX_SIZE
	mov rdi, rsp
	mov rsi, MAX_SIZE
	call read_word
	test rax, rax
	jz .size_error

	mov rdi, rsp
	mov rsi, tag
	call find_word
	test rax, rax
	jz .not_found_error

	mov rdi, rax
	add rdi, SHIFT
	call string_length
	inc rax
	add rdi, rax
	call print_string
    call print_newline
	mov rdi, 0
	call exit

.size_error:
    mov rdi, buffer_size_error
    call print_error
	mov rdi, SIZE_ERROR
	call exit

.not_found_error:
    mov rdi, not_found
    call print_error
	mov rdi, NOT_FOUND_ERROR
	call exit



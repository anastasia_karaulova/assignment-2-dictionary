section .text
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global print_int
global string_copy
global print_error
 
%define SYS_WRITE 1
%define STDERR 2

exit: 
    mov rax, 60
    syscall
 
 string_length:
    xor rax, rax 
    .cycle: 
        cmp byte [rdi+rax], 0  
        je .end 
        inc rax 
        jmp .cycle 
    .end:
        ret


print_string:
    mov rsi, rdi
    push rdi
    call string_length
    pop rdi
    mov rdi, 1
    mov rdx, rax
    mov rax, 1
    syscall
    ret



 print_char:
     push rdi
     mov rax, 1
     mov rdi, 1
     mov rsi, rsp
     mov rdx, 1
     syscall
     pop rdi
     ret


 print_newline:
     mov rdi, `\n`
     jmp print_char


print_uint:
    push    rbx
    mov     r8, rsp
    dec     rsp
    mov     byte [rsp], 0
    mov     rax, rdi
    mov     rbx, 10
    
.loop:
    xor     rdx, rdx
    div     rbx
    add     rdx, 0x30
    dec     rsp
    mov     [rsp], dl
    cmp     rax, 0
    jne     .loop   
    mov     rdi, rsp
    call    print_string 
    mov     rsp, r8
    pop     rbx
    ret



 print_int:
     test rdi, rdi
     jns print_uint
     push rdi
     mov rdi, '-'
     call print_char
     pop rdi
     neg rdi
     jmp print_uint



 string_equals:
     xor rax, rax
     xor rcx, rcx
     .loop:
       mov r8b, [rdi + rcx]
 	   mov r9b, [rsi + rcx]
 	   cmp r8b, r9b
  	   jne .end
 	   cmp byte[rdi + rcx], 0
 	   je .success
         inc rcx
 	   jmp .loop
     .success:
         inc rax
         jmp .end
     .end:
         ret



 read_char:
     xor rax, rax
     mov rdi, 0
     push 0
     mov rsi, rsp
     mov rdx, 1
     syscall
     pop rax
     ret



 read_word:
     mov     rdx, 0
 .loop:  
     push    rsi
     push    rdi
     push    rdx
     call    read_char
     pop     rdx
    pop     rdi
     pop     rsi

     cmp     rax, `\0`
     je      .end

     cmp     rax, ` `
     je      .begin
     cmp     rax, `\n`
     je      .begin
     cmp     rax, `\t`
     je      .begin
     mov     [rdx + rdi], al
     inc     rdx

     cmp     rdx, rsi
     je      .check_size
     jne     .loop
 .begin:
     cmp     rdx, 0
     je      .loop
     jne     .end
 .check_size:
     xor     rax, rax
     ret
 .end:
     mov     rax, rdi
     mov     byte [rdx + rdi], 0
     ret

  
    

 parse_uint:
     xor rax, rax
     xor rdx, rdx
     xor r10, r10
     mov r11, 10
     .loop:
         mov r10b, byte[rdi+rdx]
         cmp r10b, '0'
         jl .result
         cmp r10b, '9'
         jg .result
         push rdx
         mul r11
         pop rdx
         sub r10b, '0'
         add rax, r10
         inc rdx
         jmp .loop
     .result:
         ret


 parse_int:
     mov al, byte [rdi]
 	cmp al, '-'
 	je .sign
 	jmp parse_uint

 .sign:
 	inc rdi
 	call parse_uint
 	neg rax
 	test rdx, rdx
 	jz .err
 	inc rdx
     ret

 .err:
 	xor rax, rax
 	ret



 string_copy:
     xor rax, rax
     .loop:
         cmp rax, rdx
         je .bad
         cmp byte[rdi+rax], 0
         je .end
         mov r8b, [rdi+rax]
         mov byte[rsi+rax], r8b
         inc rax
         jmp .loop

     .end:
         mov byte[rsi+rax], 0
         ret

     .bad:
         xor rax, rax
         ret


print_error:
    push rdi
    call string_length
    pop  rsi
    mov  rdx, rax
    mov  rax, SYS_WRITE
    mov  rdi, STDERR
    syscall



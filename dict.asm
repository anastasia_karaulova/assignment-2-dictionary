extern string_equals
%define SHIFT 8

section .text
global find_word

find_word:
	push r12
	push r13
.loop:
    mov     r12, rsi
	mov     r13, rdi
    add     rsi, SHIFT
    call    string_equals
    mov     rsi, r12
    mov     rdi, r13
	
    cmp     rax, 1
    je      .success

    cmp     qword[rsi], 0
    je     .not_found

    mov     rsi, [rsi]
    jmp     .loop

.not_found:
    xor     rsi, rsi
	
.success:
    mov     rax, rsi
	pop     r13
	pop     r12
    ret
